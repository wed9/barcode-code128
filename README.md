# barcode-code128

#### 介绍
一个轻量的code128条码js库，可适用微信小程序

#### 软件架构
原生 javascript


#### 安装教程

在小程序中引入
```js
var barcode = require('../../utils/barcode.js');
```


#### 使用说明
在 wxml 文件中写明元素
```html
<canvas type="2d" id="myCanvas"></canvas>
```
在 js 文件中使用
```js 
drawBarcode() {
            const query = wx.createSelectorQuery()
            query.select('#myCanvas')
                .fields({node: true, size: true})
                .exec((res) => {
                    const canvas = res[0].node
                    barcode.code128(canvas, "1828169438248042496")
                })
        }
```